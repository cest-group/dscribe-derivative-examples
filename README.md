# DScribe derivative examples

This repository contains two examples of using the atomistic descriptor package [DScribe](https://github.com/SINGROUP/dscribe) to generate descriptor derivatives for materials science machine learning tasks. The first example uses the MBTR descriptor and its derivatives for structural optimization of perovskite alloys. The second example utilizes the SOAP descriptor and its derivatives to fit a machine learning potential for a small Cu cluster.

The examples are contained in the folders `mbtr_demo` and `soap_demo`. Both folders include a Jupyter notebook with the example code. The examples use version 2.0.0 of DScribe. This DScribe version  and the other packages required for running the codes can be installed using PIP:
```
python -m pip install -r requirements.txt
