This folder contains processed density functional theory (DFT) data of CsPb(Cl/Br)3 perovskite alloy. The calculations were performed for a study of CsPb(Cl/Br)3 stability, where the DFT data was used to train an energy predicting machine learning model. The raw data in is available at NOMAD (DOI:10.17172/NOMAD/2022.10.06-1). The code related to the study is available through GitLab (https://gitlab.com/cest-group/learnsolar-cspbclbr).

The data is divided into four data sets:

cspbclbr_single_point
Single point DFT calculations of 10 000 algorithmically generated CsPb(Cl,Br)3 structures of four different space groups: Pm-3m, P4/mbm, I4/mcm, and Pnma. The Cl concentration follows binomial distribution. Lattice parameters and atomic positions are determined through Vegard's law. Cl/Br configurations are randomized and random deviations have been added to Cs positions as well as the tilting angles of the Pb coordination octahedra.

cspbclbr_single_point_tails
Extends on cspbclbr_single_point by adding structures at the ends of the Cl/Br concentration range. Union of these two data sets has at least 100 atomic structures per Cl/Br concentration level for all four lattice types.

cspbclbr_relax_uniform
Structure snapshots from 200 DFT relaxations. Initial structures for the relaxations were generated similarly to the single point data sets. Covers equally lattice types Pm-3m, P4/mbm, I4/mcm, and Pnma. The Cl concentration range is covered uniformly. Has atomic forces for each structure.

cspbclbr_endpoints
DFT relaxed pure CsPbCl3 and CsPbBr3 structures. Four different space groups: Pm-3m, P4/mbm, I4/mcm, and Pnma.
